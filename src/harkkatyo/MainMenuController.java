
package harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Jati
 */
public class MainMenuController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private Button SPButton;
    
    @FXML
    private WebView webViewer;
    
    @FXML
    private ComboBox<SmartPost> SPCombo;
    @FXML
    private Button createPackageButton;
    @FXML
    private ComboBox<SmartPost> cityCombo;
    @FXML
    private ComboBox<StorageElement> storageCombo;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button clearButton;
    @FXML
    private Button refreshStorageButton;
    @FXML
    private Text packageBrokenText;

    //Creating the functionality for drawing the marker on the map for the automatons
    @FXML
    void listSP(ActionEvent event) {
        SmartPost output = SPCombo.getSelectionModel().getSelectedItem();
            
        String address = output.getAddress();
        String code = output.getCode();
        String city = output.getCity();
        String availability = output.getAvailability();

        String wholeAddress = address + ", " + code + " " + city;

        String input = "document.goToLocation(" + "'" + wholeAddress + "'" + ", " + "'" + availability + "', 'red')";

        webViewer.getEngine().executeScript(input);
        
    }
    
    //Initializating the index.html file and SmartPost automatons for the ComboBox
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webViewer.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        try {
            SmartPostData sp = SmartPostData.getInstance();
            ArrayList<ArrayList<SmartPost>> SPList = sp.getSpList();

            for (int i = 0; i < SPList.size(); i++){
                    cityCombo.getItems().add(SPList.get(i).get(0)); // SmartPost objects, one from each city
            }


        } catch (IOException | SAXException | ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }

        // TODO
    }    

//  openCASPWindow is Erno Vanhala's code, not our own.
    @FXML
    private void openCASPWindow(ActionEvent event) throws IOException {
        
        try {
            Stage webview = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("CreateAndSendPackage.fxml"));
            Scene scene = new Scene(page);
            webview.setScene(scene);
            
            webview.show();
            
        } catch (IOException ex) {
            Logger.getLogger(MainMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Creating the functionality for selecting the city
    @FXML
    private void selectCity(ActionEvent event) {
        SPCombo.getItems().clear();
        String city = cityCombo.getSelectionModel().getSelectedItem().getCity();
        
        try {
            SmartPostData sp = SmartPostData.getInstance();
            ArrayList<ArrayList<SmartPost>> SPList = sp.getSpList();

            for (int i = 0; i < SPList.size(); i++){
                // If selected city equals city in ArrayList<SmartPost> element
                if (SPList.get(i).get(0).getCity().equals(city)){
                    ArrayList<SmartPost> automatons = SPList.get(i);
                    for (int j = 0; j < automatons.size(); j++){
                        SmartPost automaton = SPList.get(i).get(j);
                        automaton.toStringMethod  = false;
                        SPCombo.getItems().add(automaton);
                    }
                }
            }
        } catch (IOException | SAXException | ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }

    //Functionality of the send package button, calculating the coordinates, creating the string for JavaScript API
    @FXML
    private void sendPackage(ActionEvent event) {
        StorageElement paketti = storageCombo.getSelectionModel().getSelectedItem();
        // Delete package from storage
        storageCombo.getItems().remove(paketti);
        
        SmartPost initLocation = paketti.getSpInit();
        float initLatitude = initLocation.getGp().getLatitude();
        float initLongitude = initLocation.getGp().getLongitude();
        
        SmartPost destLocation = paketti.getSpDest();
        float destLatitude = destLocation.getGp().getLatitude();
        float destLongitude = destLocation.getGp().getLongitude();
        
        ArrayList<Float> coordList = new ArrayList<>();
        coordList.add(initLatitude);
        coordList.add(initLongitude);
        coordList.add(destLatitude);
        coordList.add(destLongitude);
        
        int deliveryClass = paketti.getPaketti().getDeliveryClass();
        
        String input = "document.createPath(" + coordList + ", 'red', " + deliveryClass + ")";
        webViewer.getEngine().executeScript(input);
        
        //Notify the user if the item breaks
        Double breakage = (double)ThreadLocalRandom.current().nextInt(0, 101);
        breakage = breakage/100;
        if (breakage < paketti.getPaketti().getBreakingChance()) {
            packageBrokenText.setOpacity(1);
            
        }
        
        
    }
    //Package breaking error message is hidden when selecting a new package from the list
    @FXML
    void clearError(ActionEvent event) {
        packageBrokenText.setOpacity(0);
    }
    //Clear roads button functionality
    @FXML
    private void clearRoads(ActionEvent event) {
        webViewer.getEngine().executeScript("document.deletePaths()");
    }

    //Refreshing the storage ComboBox - we couldn't figure out how to do it automatically
    @FXML
    private void refreshStorage(ActionEvent event) {
        try {
            Storage sto = Storage.getInstance();
            ArrayList<StorageElement> packages = sto.getList();
            storageCombo.getItems().clear();

            for (int i = 0; i < packages.size(); i++){
                    storageCombo.getItems().add(packages.get(i)); // SmartPost objects, one from each city
            }
        } catch (IOException | SAXException | ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }
}
