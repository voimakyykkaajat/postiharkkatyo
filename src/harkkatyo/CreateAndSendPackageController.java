/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author Jati
 */
public class CreateAndSendPackageController implements Initializable {

    @FXML
    private Button createButton;
    @FXML
    private ComboBox<Item> itemCombo;
    @FXML
    private ComboBox<SmartPost> InitCCombo; // initial city
    @FXML
    private ComboBox<SmartPost> initACombo; // initial automaton
    @FXML
    private ComboBox<SmartPost> destCCombo; // destination
    @FXML
    private ComboBox<SmartPost> destACombo;
    @FXML
    private TextField itemName;
    @FXML
    private TextField itemSize;
    @FXML
    private TextField itemWeight;
    @FXML
    private CheckBox breakableCheck;
    @FXML
    private Button clearFieldsButton;
    @FXML
    private CheckBox FirstClassCheck;
    @FXML
    private CheckBox SecondClassCheck;
    @FXML
    private CheckBox ThirdClassCheck;
    @FXML
    private Button InfoButton;
    @FXML
    private Text sizeErrorText;
    @FXML
    private Text weightErrorText;
    @FXML
    private Text selectionErrorText;
    @FXML
    private Text itemErrorText;
    @FXML
    private Text distanceErrorText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //Creating items for the combobox
        Beer item1 = new Beer();
        CigarBox item2 = new CigarBox();
        MlpCollection item3 = new MlpCollection();
        Rock item4 = new Rock();    

        itemCombo.getItems().add(item1);
        itemCombo.getItems().add(item2);
        itemCombo.getItems().add(item3);
        itemCombo.getItems().add(item4);
        
        //Initialize SmartPost data for the comboboxes
        try {
            SmartPostData sp = SmartPostData.getInstance();
            ArrayList<ArrayList<SmartPost>> SPList = sp.getSpList();

            for (int i = 0; i < SPList.size(); i++){
                    InitCCombo.getItems().add(SPList.get(i).get(0)); // SmartPost objects, one from each city
                    destCCombo.getItems().add(SPList.get(i).get(0)); // SmartPost objects, one from each city
            }


        } catch (IOException | SAXException | ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }    

    @FXML
    private void createPackage(ActionEvent event) throws IOException, SAXException, ParserConfigurationException {
        // Creating selItem, which is the item to be packaged
        Item selItem;
        // Check if something is selected in the comboboxes
        if (initACombo.getSelectionModel().isEmpty() || destACombo.getSelectionModel().isEmpty()) {
            selectionErrorText.setOpacity(1);
            return;
        }
        
        //clear errors
        selectionErrorText.setOpacity(0);
        itemErrorText.setOpacity(0);
        sizeErrorText.setOpacity(0.0);
        
        
        //If the combo box does not have anything selected and any of the fields is empty then item is not created
        if (itemCombo.getSelectionModel().isEmpty() && (itemName.getText().equals("") || itemSize.getText().equals("") || itemWeight.getText().equals(""))){
            itemErrorText.setOpacity(1);
            return;
        }
        
        // Initializing objects for comparing (to find out if item is too big for a package class)
        FirstClass compare1 = new FirstClass(2.0);
        SecondClass compare2 = new SecondClass(5.0);
        ThirdClass compare3 = new ThirdClass(15.0);
        
        //if the fields are empty, the item is selected from the combobox
        if (itemName.getText().equals("") && itemSize.getText().equals("") && itemWeight.getText().equals("")){
            selItem = itemCombo.getSelectionModel().getSelectedItem();
        }
        //else the item is created from the fields
        else {
            String name = itemName.getText();
            String size = itemSize.getText();
            String weightS = itemWeight.getText();
            Double weight = Double.parseDouble(weightS);
            
            String[] sizeList = size.split("x");
            
            String lengthS = sizeList[0].trim();
            Double length = Double.parseDouble(lengthS);
            
            String widthS = sizeList[1].trim();
            Double width = Double.parseDouble(widthS);
            
            String heigthS = sizeList[2].trim();
            Double heigth = Double.parseDouble(heigthS);
            
            Boolean breakable = breakableCheck.isSelected();
            
            
            selItem = new UserItem(name, length, width, heigth, weight, breakable);
        }
        
        // Checking which class is selected for the packaging and sending
        
        if (FirstClassCheck.isSelected()){
            //Check if the distance is <150 km
            SmartPost spInit = initACombo.getSelectionModel().getSelectedItem();
            SmartPost spDest = destACombo.getSelectionModel().getSelectedItem();
            
            float initLatitude = spInit.getGp().getLatitude();
            float initLongitude = spInit.getGp().getLongitude();
            
            float destLatitude = spDest.getGp().getLatitude();
            float destLongitude = spDest.getGp().getLongitude();
            
            Double distance = calcDistance(initLatitude, initLongitude, destLatitude, destLongitude);
            
            //Check all of the measurements of the item so that it is suitable for the selected package class
            if (distance < compare1.maxDistance) {
                for(int i=0;i<3;i++) {
                    if (compare1.getSize().get(i) < selItem.getSize().get(i)) {
                        sizeErrorText.setOpacity(1.0);
                        return;
                    }

                    if (compare1.getWeight() < selItem.getWeight()) {
                        weightErrorText.setOpacity(1.0);
                        return;
                    }
                }
                //Creating the package and inserting it to storage
                Package paketti = new FirstClass(selItem.getWeight());

                Storage sto = Storage.getInstance();
                sto.addPackageToStorage(spInit, spDest, paketti, selItem);
            }
            else{
                distanceErrorText.setOpacity(1);
                return;
            }
            
        }
        //Same as earlier for the second class
        else if (SecondClassCheck.isSelected()){
            
            for(int i=0;i<3;i++) {
                if (compare2.getSize().get(i) < selItem.getSize().get(i)) {
                  sizeErrorText.setOpacity(1.0);
                  return;
              }

              if (compare2.getWeight() < selItem.getWeight()) {
                  weightErrorText.setOpacity(1.0);
                  return;
              }

            }
            
            Package paketti = new SecondClass(selItem.getWeight());
            SmartPost spInit = initACombo.getSelectionModel().getSelectedItem();
            SmartPost spDest  = destACombo.getSelectionModel().getSelectedItem();
            
            Storage sto = Storage.getInstance();
            sto.addPackageToStorage(spInit, spDest, paketti, selItem);
        }
        
        //Same as earlier for the third class
        else if (ThirdClassCheck.isSelected()){
            
            for(int i=0;i<3;i++) {
                if (compare3.getSize().get(i) < selItem.getSize().get(i)) {
                  sizeErrorText.setOpacity(1.0);
                  return;
              }

              if (compare3.getWeight() < selItem.getWeight()) {
                  weightErrorText.setOpacity(1.0);
                  return;
              }

            }
            Package paketti = new ThirdClass(selItem.getWeight());
            SmartPost spInit = initACombo.getSelectionModel().getSelectedItem();
            SmartPost spDest  = destACombo.getSelectionModel().getSelectedItem();

            Storage sto = Storage.getInstance();
            sto.addPackageToStorage(spInit, spDest, paketti, selItem);
        }
    }
    
    //CLear fields button
    @FXML
    private void clearFields(ActionEvent event) {
        itemName.clear();
        itemSize.clear();
        itemWeight.clear();
    }
    //Functionality for the opening info button
    @FXML
    private void openCASPWindow(ActionEvent event) throws IOException {
        
        try {
            Stage webview = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("PackageClassInfo.fxml"));
            Scene scene = new Scene(page);
            webview.setScene(scene);
            
            webview.show();
            
        } catch (IOException ex) {
            Logger.getLogger(MainMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //If first class tick box is selected, other boxes are emptied
    @FXML
    private void FirstClassClearOther(ActionEvent event) {
        sizeErrorText.setOpacity(0);
        weightErrorText.setOpacity(0);
        
        SecondClassCheck.setSelected(false);
        ThirdClassCheck.setSelected(false);
        
        if (!FirstClassCheck.isSelected()){
            FirstClassCheck.setSelected(true);
        }
    }

    //If the second class tick box is selected, other boxes are emptied
    @FXML
    private void SecondClassClearOther(ActionEvent event) {
        sizeErrorText.setOpacity(0);
        weightErrorText.setOpacity(0);
        distanceErrorText.setOpacity(0);
        
        FirstClassCheck.setSelected(false);
        ThirdClassCheck.setSelected(false);
        
        if (!SecondClassCheck.isSelected()){
            SecondClassCheck.setSelected(true);
        }
    }
    
    //IF the third class tick box is selected, other boxes are emptied
    @FXML
    private void ThirdClassClearOther(ActionEvent event) {
        sizeErrorText.setOpacity(0);
        weightErrorText.setOpacity(0);
        distanceErrorText.setOpacity(0);
        
        FirstClassCheck.setSelected(false);
        SecondClassCheck.setSelected(false);
        
        if (!FirstClassCheck.isSelected()){
            ThirdClassCheck.setSelected(true);
        }
    
    }
    //Creating the functionality for selecting the initial city
    @FXML
    private void selectInitCity(ActionEvent event) {
        initACombo.getItems().clear();
        String city = InitCCombo.getSelectionModel().getSelectedItem().getCity();
        
        try {
            SmartPostData sp = SmartPostData.getInstance();
            ArrayList<ArrayList<SmartPost>> SPList = sp.getSpList();

            for (int i = 0; i < SPList.size(); i++){
                // If selected city equals city in ArrayList<SmartPost> element
                if (SPList.get(i).get(0).getCity().equals(city)){
                    ArrayList<SmartPost> automatons = SPList.get(i);
                    for (int j = 0; j < automatons.size(); j++){
                        SmartPost automaton = SPList.get(i).get(j);
                        automaton.toStringMethod  = false;
                        initACombo.getItems().add(automaton);
                    }
                }
            }
        } catch (IOException | SAXException | ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }
    //Creating the functionality for selecting the destination city
    @FXML
    private void selectDestCity(ActionEvent event) {
        destACombo.getItems().clear();
        String city = destCCombo.getSelectionModel().getSelectedItem().getCity();
        
        try {
            SmartPostData sp = SmartPostData.getInstance();
            ArrayList<ArrayList<SmartPost>> SPList = sp.getSpList();

            for (int i = 0; i < SPList.size(); i++){
                // If selected city equals city in ArrayList<SmartPost> element
                if (SPList.get(i).get(0).getCity().equals(city)){
                    ArrayList<SmartPost> automatons = SPList.get(i);
                    for (int j = 0; j < automatons.size(); j++){
                        SmartPost automaton = SPList.get(i).get(j);
                        automaton.toStringMethod  = false;
                        destACombo.getItems().add(automaton);
                    }
                }
            }
        } catch (IOException | SAXException | ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }
    
// Distance between two coordinates in kilometres.
// Code got from https://stackoverflow.com/questions/5557706/calculating-distance-using-latitude-longitude-coordinates-in-kilometers-with-jav
    private Double calcDistance(float latA, float longA, float latB, float longB) {

        double theDistance = (Math.sin(Math.toRadians(latA)) *
                Math.sin(Math.toRadians(latB)) +
                Math.cos(Math.toRadians(latA)) *
                Math.cos(Math.toRadians(latB)) *
                Math.cos(Math.toRadians(longA - longB)));

        return new Double((Math.toDegrees(Math.acos(theDistance))) * 69.09*1.609);
    }
}
