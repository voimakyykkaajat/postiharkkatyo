// Olio-ohjelmointi Assignment. Creating a system for Smart Post. Lappeenranta University of Technology. 2017-12-15.
// Authors: Heimo Hiidenkari 0417489, Jaakko Timonen 0418077

package harkkatyo;

public class GeoPoint {
	private float latitude;
	private float longitude;

	public GeoPoint(float latitude, float longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

}
