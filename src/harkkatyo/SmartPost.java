// Olio-ohjelmointi Assignment. Creating a system for Smart Post. Lappeenranta University of Technology. 2017-12-15.
// Authors: Heimo Hiidenkari 0417489, Jaakko Timonen 0418077

package harkkatyo;

public class SmartPost {

	private String city;
        private String code;
	private String address;
	private String availability;
	private String postoffice;
	private GeoPoint gp;
        public boolean toStringMethod = true;

//	Constructor
//	Data is inserted from SmartPostData
	public SmartPost(String city, String code, String address, String availability, String postoffice, float latitude, float longitude){
		this.city = city;
                this.code = code;
		this.address = address;
		this.availability = availability;
		this.postoffice = postoffice;
		gp = new GeoPoint(latitude, longitude);
	}

	public String getCity() {
		return city;
	}

	public String getAddress() {
		return address;
	}

	public String getAvailability() {
		return availability;
	}

	public String getPostoffice() {
		return postoffice;
	}

	public GeoPoint getGp() {
		return gp;
	}
        
        public String getCode() {
		return code;
	}

   
        @Override
        public String toString(){
            if(toStringMethod){
                return city;
            }
            else{
                return postoffice;
	    }
        }

}
