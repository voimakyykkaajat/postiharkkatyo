/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

/**
 *
 * @author Jati
 */
//The storage elements takes in the initial SmartPost, destionation SmartPost, Package and Item objects.
public class StorageElement {
    private SmartPost spInit;
    private SmartPost spDest;
    private Package paketti;
    private Item selItem;

    public SmartPost getSpInit() {
        return spInit;
    }

    public SmartPost getSpDest() {
        return spDest;
    }

    public Package getPaketti() {
        return paketti;
    }

    public Item getSelItem() {
        return selItem;
    }
    
    public StorageElement(SmartPost spInit, SmartPost spDest, Package paketti, Item selItem){
        this.spInit = spInit;
        this.spDest = spDest;
        this.paketti = paketti;
        this.selItem = selItem;
    }
    //Creating the beautiful string for the ComboBox in the main menu consisting of the packages to be sent
    public String toString(){
        String returner = selItem.itemName + " from " +
                spInit.getCity() + " to " + 
                spDest.getCity();
        return returner;
    }
}
