// Olio-ohjelmointi Assignment. Creating a system for Smart Post. Lappeenranta University of Technology. 2017-12-15.
// Authors: Heimo Hiidenkari 0417489, Jaakko Timonen 0418077

//  The XML loading and parsing procedure is originally Erno Vanhala's code. We have modified it to match our needs.
//	The source for the original code is below
//	https://bitbucket.org/evanhala/olio-ohjelmointi/commits/47e43c7b33fae3eb8e6da5583ea67b51f0e857f0

package harkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SmartPostData {

//	Initialization of variables.

	// We want a list that has all smart post automatons of each city stored.
	// This is done with an array list that has array lists consisting of SmartPost objects as its elements.
	private ArrayList<ArrayList<SmartPost>> spList = new ArrayList<>();
	private static SmartPostData db = null;
	private String url = "http://smartpost.ee/fi_apt.xml";	 // XML data
	private Document doc;

//	Constructor
//	XML data is read and stored to variable content.
//	Content is read to variable doc and normalized.
//	Data in doc is split to nodes by the key word "place". Each node is gone through
//	separately and data in the nodes are stored as SmartPost objects to variable subList.
//	When the city is changed, subList is stored to spList and subList is cleared.
	public SmartPostData() throws IOException, SAXException, ParserConfigurationException{
		String content = loadXML(url);
		parseXML(content);
		parseMyData();
	}

//	Singleton principle used on SmartPostData.
	static public SmartPostData getInstance() throws IOException, SAXException, ParserConfigurationException {
            if (db == null){
                    db = new SmartPostData();
            }
            return db;
        }

	public ArrayList<ArrayList<SmartPost>> getSpList() {
		return spList;
	}

//	Function that loads the XML data as String from the site in the variable url.
//	Stores the data in variable content.
	private String loadXML(String url) throws IOException{
		URL urli = new URL(url);
		BufferedReader br  = new BufferedReader(new InputStreamReader(urli.openStream(), "UTF-8"));

		String content = "";
		String line;

		while((line = br.readLine()) != null){
			content += line + "\n";
		}

		return content;
	}

//	Function that generates a document builder to parse content to type Document.
//	The Document doc is then normalized so that its elements will be easily accessible.
	public void parseXML(String content) throws SAXException, IOException, ParserConfigurationException{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

		doc = dBuilder.parse(new InputSource(new StringReader(content)));
		doc.getDocumentElement().normalize();
	}

//	Function that allocates data in variable doc to variables in SmartPost objects.
//	SmartPost objects are stored to variable SPList.
	private void parseMyData(){
		// Creates node list, so splits the data in doc to nodes with tag name "place".
		NodeList nodes = doc.getElementsByTagName("place");

		ArrayList<SmartPost> subList = new ArrayList<>(); // subList initialized

		// Äänekoski is the city of the first element in the XML content.
		// It is needed as initialization.
		String cityCompare = "Äänekoski";

		// Each node is gone through
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			String city = getValue("city", e);  // Information is gathered using specific tag names of the XML content.
			city = city.substring(0, 1) + city.substring(1).toLowerCase();	// Capitalization


			// When cityCompare and current city are not equal, cityCompare is changed to match city.
			// Then the current subList is added to spList and subList is initialized for the next city.
			if (!city.equals(cityCompare)){
				cityCompare = city;

				spList.add(subList);
				subList = new ArrayList<>();
			}

			// When cityCompare and current city are equal, SmartPost objects are made and
			// they are added to subList.
			if (city.equals(cityCompare)){

				// Information is gathered using specific tag names of the XML content.
                                String code = getValue("code", e);
				String address = getValue("address", e);
				String availability = getValue("availability", e);
				String postoffice = getValue("postoffice", e);
				String latitudeS = getValue("lat", e);
				String longitudeS = getValue("lng", e);

				// Parsing of data to more desirable forms.
				 // "SmartPost, " and "Pakettiautomaatti, " are erased from postoffice.
				if (postoffice.toLowerCase().contains("smartpost") || postoffice.toLowerCase().contains("pakettiautomaatti")){
					if (postoffice.contains(",")){
						postoffice = postoffice.split(",")[1].trim();
					}
				}
				float latitude = Float.parseFloat(latitudeS);
				float longitude = Float.parseFloat(longitudeS);

				// Creation of SmartPost objects and adding them to subList.
				SmartPost sp = new SmartPost(city, code, address, availability, postoffice, latitude, longitude);
				subList.add(sp);
			}
		}
	}

//	Function that fetches the text content of an element. An element looks like this
//	<tag>This Is The Text Content</tag>
	private String getValue(String tag, Element e) {
		return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
	}

	public ArrayList<ArrayList<SmartPost>> sortSPList(ArrayList<ArrayList<SmartPost>> spList){

		ArrayList<ArrayList<SmartPost>> list = spList;
		Boolean flag = false;

		while (true){

			for (int i = 0; i < (list.size() - 1); i++){
				String city1 = list.get(i).get(0).getCity();
				String city2 = list.get(i+1).get(0).getCity();

				if ((city2.compareTo(city1)) == 1){
					ArrayList<SmartPost> sp1 = list.get(i);
					ArrayList<SmartPost> sp2 = list.get(i+1);

					list.set(i, sp2);
					list.set(i+1, sp1);

					flag = true;
				}
			}

			if (!flag){
				break;
			}
			flag = false;

		}
		return list;

	}
}
