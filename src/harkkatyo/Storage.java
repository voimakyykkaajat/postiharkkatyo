/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

//Creaitng arraylist for the storage. Only one storage is used but it consists of Storage Elements!
public class Storage {
    
    static private Storage sto = null;
    ArrayList<StorageElement> packages = new ArrayList<>();
    
    public Storage(){
        
    }
    
//  Singleton principle used on SmartPostData.
    static public Storage getInstance() throws IOException, SAXException, ParserConfigurationException {
        if ( sto == null){
                sto = new Storage();
        }
        return sto;
    }
        
    public void addPackageToStorage(SmartPost spInit, SmartPost spDest, Package paketti, Item selItem){
        StorageElement se = new StorageElement(spInit,spDest,paketti,selItem);
        packages.add(se);
    }
    
    public ArrayList<StorageElement> getList(){
        return packages;
    }
}
