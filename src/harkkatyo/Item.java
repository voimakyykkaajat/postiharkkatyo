/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Jati
 */

public abstract class Item {
    protected String itemName;
    protected ArrayList<Double> size = new ArrayList<Double>();
    protected Boolean breakable;
    protected Double weight;
    protected Double length;
    protected Double width;
    protected Double height;
        
    @Override
    public String toString(){
        return itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public ArrayList<Double> getSize() {
        return size;
    }

    public Boolean getBreakable() {
        return breakable;
    }

    public Double getWeight() {
        return weight;
    }        
    }
    
    class MlpCollection extends Item {
        public MlpCollection(){
        this.itemName = "My little pony Collection";
        this.size.add(59.0);
        this.size.add(30.0);
        this.size.add(30.0);
        this.breakable = true;
        this.weight = 1.0;
        }
    }
    
    class CigarBox extends Item {
        public CigarBox(){
            this.itemName = "A wooden box of cigars";
            this.size.add(30.0);
            this.size.add(20.0);
            this.size.add(10.0);
            this.breakable = false;
            this.weight = 0.5;
        }
    }

    class Beer extends Item {
        public Beer(){
            this.itemName = "A box full of beer cans";
            this.size.add(40.0);
            this.size.add(26.5);
            this.size.add(12.0);
            this.breakable = true;
            this.weight = 8.0;
        }
    }

    class Rock extends Item {
        public Rock(){
            this.itemName = "A rock";
            this.size.add(20.0);
            this.size.add(10.0);
            this.size.add(10.0);
            this.breakable = false;
            this.weight = 20.0;
        }
    }
       

    class UserItem extends Item{
        public UserItem(String itemName, Double length, Double width, Double height, Double weight, Boolean breakable) {
            this.itemName = itemName;
            this.size.add(length);
            this.size.add(width);
            this.size.add(height);
            Collections.sort(size);
            Collections.reverse(size);
            this.weight = weight;
            this.breakable = breakable;
            }
    


    }