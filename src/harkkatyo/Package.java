/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkkatyo;

import java.util.ArrayList;

/**
 *
 * @author Jati
 */

public abstract class Package {
        private Double weight;
	protected Double breakingChance;
        protected ArrayList<Double> size = new ArrayList<Double>();
        protected int deliveryClass;

    public Double getBreakingChance() {
        return breakingChance;
    }
    
    public Double getWeight(){
        return weight;
    }

    public int getDeliveryClass() {
        return deliveryClass;
    }
    
    public ArrayList<Double> getSize() {
        return size;
    }

	public Package(Double weight) {

                this.weight = weight;

	}
}

class FirstClass extends Package {
    
        protected Double maxDistance;
        

	public FirstClass(Double weight) {
		super(weight);
                
                deliveryClass = 1;
                maxDistance = 150.0;
                breakingChance = 1.0;
                
                this.size.add(100.0);
                this.size.add(60.0);
                this.size.add(60.0);
                
                
	}


}

class SecondClass extends Package {

	public SecondClass(Double weight) {
		super(weight);
		
                deliveryClass = 2;
                breakingChance = 0.0;
                
                this.size.add(60.0);
                this.size.add(36.0);
                this.size.add(19.0);
                
	}
}

class ThirdClass extends Package {
    
        private Double maxWeight;
    
        public ThirdClass(Double weight) {
                super(weight);
                
                deliveryClass = 3;
                maxWeight = 10.0;
                //Creating dynamic breaking chance - breaking chance is dependant on the weight
                breakingChance = 1-(weight/maxWeight);
                
                this.size.add(60.0);
                this.size.add(59.0);
                this.size.add(36.0);
        }
    
        
    
        
}
